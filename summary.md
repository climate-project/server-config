# Bias Correction of Multi-Dimensional Climate Data and Visualization Deploy Document

## Repository OS and Program version
- repository: https://gitlab.com/climate-project/app
- Cent OS 7
- Python 3.6.8
- Node 12.16.3
- Mongodb 4.0.18
- nginx 1.16.1

## เปิด Port และทำการ forward port ที่ต้องใช้

ในที่นี้ flask ใช้ port 3000 และ angular port 4200
เมื่อเรียกใช้งานจากภายนอก flask ใช้ port 7553 และ angular ใช้ port 7554
```sh
sudo firewall-cmd --zone=public --add-port=4200/tcp --permanent
sudo firewall-cmd --reload
```

## เตรียม backend
- ทำการ import metadata ลง database 
  - แก้ไข path ไปยังไฟล์ nc แล้วทำการรันไฟล์
    - `flask-api/app/ncfiles/import_metadata_raw_ncrcm.py`
    - `flask-api/app/ncfiles/import_metadata_indices_nc.py`
  - แก้ไข/เพิ่มเติมข้อมูลเกี่ยวกับ index ได้ในไฟล์ `flask-api/app/ncfiles/_data/index_definitions.csv` แล้วทำการรันไฟล์
  - `flask-api/app/ncfiles/import_index_details.py`
- แก้ไขตำแหน่งที่เก็บไฟล์ nc
    - `flask-api/app/nc_routes.py` (ข้อมูลดิบ)
    - `flask-api/app/ncindices_routes.py` (ข้อมูล index)
  
##  ขั้นตอนการ deploy backend
### clone project , สร้าง virtual environment และ ลง gunicorn
```sh
cd project_folder 
python3 -m venv projectvenv
source projectvenv/bin/activate
```
สังเกตว่าจะมีชื่อ virtual environment `projectenv` นำหน้าชื่อ user ของ shell
```sh
pip3 install gunicorn
```
### สร้างไฟล์ `production.py` สำหรับให้ gunicorn เรียกใช้งาน
```sh
from app import app

if __name__ == "__main__":
    app.run(host="0.0.0.0", port="3000", threaded=True)
```
ลองทำการเปิดเซิฟเวอร์ด้วย gunicorn หรือเปิดด้วย flask เพื่อทดสอบการเรียกใช้ api จากภายนอก

จากตัวอย่าง gunicorn เรียกใช้งานไฟล์ `production` ต้องทำการตั้งค่า timeout ไว้
เนื่องจากมีการคำนวณที่นาน ถ้าหากไม่ตั้งค่าไว้ เมื่อเรียก api จะ `empty response`
```sh
gunicorn --bind 0.0.0.0:3000 production:app --timeout 600 --workers 5 --threads 2

python3 production.py
```

 ### สร้างไฟล์ `/etc/systemd/system/climatebackend.service` สำหรับเรียกใช้งาน gunicorn เพื่อเปิดเซิฟ backend ทุกครั้งที่เครื่องเซิฟเวอร์ restart
```sh 
sudo nano /etc/systemd/system/climatebackend.service
 ```

```
[Unit]
Description=Gunicorn climate project backend server
After=network.target

[Service]
User=chuan
WorkingDirectory=/home/chuan/flask-api
ExecStart=/home/chuan/flask-api/projectvenv/bin/gunicorn --bind 0.0.0.0:3000 production:app --timeout 600 --workers 5 --threads 2
Restart=always

[Install]
WantedBy=multi-user.target
 ```

### เรียกใช้งานไฟล์ service ที่สร้างไว้
```
sudo systemctl daemon-reload
sudo systemctl start climatebackend
sudo systemctl enable climatebackend

sudo systemctl status climatebackend   # ตรวจสอบสถานะของ service
```

## เตรียม frontend
- แก้ไข api url ของ backend ในไฟล์ เป็นดังนี้
  - `angular-frontend/src/app/export.service.ts`
  - `angular-fronend/src/app/input.service.ts`
  - `angular-frontend/src/app/map.service.ts`
```
http://serverdomain.net:7553/api (เปิดให้ flask เรียกจากภายนอกที่พอร์ต 7553) 
```
- แก้ไขดีเลย์การรอกริดในไฟล์ `angular-frontend/src/app/input/input.component.ts` line 106
```
 .pipe(delay(500)) เป็น 5000 
```
- ทำการ build โปรเจค ผลลัพธ์การ build จะอยู่ที่ `dist/primeNG`

```sh
ng build
```

## ขั้นตอน deplot frontend
### เตรียม nginx 
```
sudo systemctl start nginx
sudo systemctl enable nginx 
```
แก้ไข port สำหรับ frontend ในไฟล์ `/etc/nginx/nginx.conf`
```
    server {
        listen       <port> default_server;
        listen       [::]:<port> default_server;
        server_name  _;
        root         /usr/share/nginx/html;
    }
```
nginx จะ error `permission denied` กับ port 4200

```
semanage port -a -t http_port_t  -p tcp 4200

(หรือใช้เป็น -m -t ถ้าพอร์ต 4200 มีอยู่แล้ว)
```

### คัดลอกไฟล์ที่ build เสร็จไปใน `/var/www/`
```
  cp -r dist/primeNG /var/www/climate_frontend
```
### ตั้งค่าในไฟล์ `/etc/nginx/nginx.conf`
```
server {
        listen       4200 default_server;
        listen       [::]:4200 default_server;
        server_name  _;
        # root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        # include /etc/nginx/default.d/*.conf;

        location / {
                root /var/www/climate_frontend;
                autoindex on;
        }
```
###  restart nginx
```
sudo nginx -s reload
sudo systemctl restart nginx
```