# server-config

```sh
cat /etc/os-release
```

## CentOS7

## ลง python3.6, install library

[install python](https://phoenixnap.com/kb/how-to-install-python-3-centos-7)

[install python](https://www.liquidweb.com/kb/how-to-install-python-3-on-centos-7/)

## ลง MONGO 4.0
[by mongodb](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-red-hat/)

```
mongorestore --db <ชื่อ db> <folder>

mongorestore --db climatedb_2019 ./climatedb_2019
```
## การทำ forwardport
[forward port](https://linuxacademy.com/guide/11630-internal-port-forwarding-on-linux-using-the-firewall/?fbclid=IwAR3uz4mOxEmzE4E9DKxwp9GsHplNOj1sGZlQ_qgCCiZg41O84TLtM28r5m4)

ทดลองแบบไม่มี `--permanent`
```
firewall-cmd --permanent --add-forward-port=port=<นอก>:proto=tcp:toport=<ใน>

นอก = user เรียกใช้ เช่น 7533
ใน = ที่ dev เปิดไว้ flask = 3000
```

```
firewall-cmd --add-forward-port=port=7553:proto=tcp:toport=3000
```

## backend deploy

- [ref](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-centos-7)
- [config more detail](/config_deploy_backend.md)
