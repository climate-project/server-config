# ลองเปิดเซิฟ
จาก helloworld flask ต้องมี `0.0.0.0` ทำ forward port จะเข้าถึงเว็ปได้
```
## `runserver.py`
from app import app

if __name__ == "__main__":
    app.run(host='0.0.0.0', port="3000", debug=True, threaded=True)
```

```
python3 runserver
```

```
serverdomain.net:7553
```


# setup backend

## forward port ไม่ต้องทำแล้ว แต่ต้องเปิด port 3000/4200 ไว้
```
sudo firewall-cmd --permanent --add-forward-port=port=7553:proto=tcp:toport=3000
firewall-cmd --reload
```

ดู ผลลัพธ์
```
sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: eth0
  sources:
  services: dhcpv6-client http ssh
  ports: 3000/tcp
  protocols:
  masquerade: no
  forward-ports: port=7553:proto=tcp:toport=3000:toaddr=
  source-ports:
  icmp-blocks:
  rich rules:
```

## clone backend project
## สร้าง virtual environment
```sh
cd project_folder 
python3 -m venv projectvenv
source projectvenv/bin/activate
```
จะเห็นว่ามี ชื่อ venv อยู่หน้า user

```
pip3 install gunicorn
```
## สร้างไฟล์ production.py
```
from app import app

if __name__ == "__main__":
    # app.run(host='0.0.0.0', port="3000", debug=True)
    app.run(host="0.0.0.0", port="3000", threaded=True)
```
## ลอง runserver ถ้าลง lib ไม่ครบก็ลงให้ครบ
```
python3 production.py
```
## เปิดเซิฟด้วย gunicorn 
```
gunicorn --bind 0.0.0.0:3000 production:app --timeout 600
```

[ตั้ง worker timeout](https://stackoverflow.com/questions/59347635/django-err-empty-response)
ลอง query ข้อมูลทีป่ระมวลผลนานจะไม่มีการตอบจาก server gunicorn ต้องตั้ง timeout ตัวอย่าง 600sec

ลองเข้า api
```
http://serverdomain.net:7553
```

## สร้างไฟล์ /etc/systemd/system/climatebackend.service ให้เปิดเซิฟทุกครั้งที่รีเครื่อง 
```
[Unit]
Description=Gunicorn climate project backend server
After=network.target

[Service]
User=chuan
WorkingDirectory=/home/chuan/flask-api
ExecStart=/home/chuan/flask-api/projectvenv/bin/gunicorn --bind 0.0.0.0:3000 production:app --timeout 600 --workers 10 --threads 2
Restart=always

[Install]
WantedBy=multi-user.target
```

## เรียกใช้งาน

```
sudo systemctl daemon-reload
sudo systemctl start climatebackend
sudo systemctl enable climatebackend
```

ตรวจว่าเซิฟรันไหม
```
sudo systemctl status climatebackend # check status
```

```
climatebackend.service - Gunicorn climate project backend server
   Loaded: loaded (/etc/systemd/system/climatebackend.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-05-20 13:29:50 +07; 2s ago
 Main PID: 1420 (gunicorn)
    Tasks: 4
   CGroup: /system.slice/climatebackend.service
           ├─1420 /home/chuan/flask-api/projectvenv/bin/python3 /home/chuan/flask-api/projectvenv/bin/gunicorn --workers 3 --bind 0.0.0.0:3000 production:app --ti...
           ├─1434 /home/chuan/flask-api/projectvenv/bin/python3 /home/chuan/flask-api/projectvenv/bin/gunicorn --workers 3 --bind 0.0.0.0:3000 production:app --ti...
           ├─1435 /home/chuan/flask-api/projectvenv/bin/python3 /home/chuan/flask-api/projectvenv/bin/gunicorn --workers 3 --bind 0.0.0.0:3000 production:app --ti...
           └─1436 /home/chuan/flask-api/projectvenv/bin/python3 /home/chuan/flask-api/projectvenv/bin/gunicorn --workers 3 --bind 0.0.0.0:3000 production:app --ti...

May 20 13:29:50 localhost.localdomain systemd[1]: Started Gunicorn climate project backend server.
May 20 13:29:50 localhost.localdomain gunicorn[1420]: [2020-05-20 13:29:50 +0700] [1420] [INFO] Starting gunicorn 20.0.4
May 20 13:29:50 localhost.localdomain gunicorn[1420]: [2020-05-20 13:29:50 +0700] [1420] [INFO] Listening at: http://0.0.0.0:3000 (1420)
May 20 13:29:50 localhost.localdomain gunicorn[1420]: [2020-05-20 13:29:50 +0700] [1420] [INFO] Using worker: sync
May 20 13:29:50 localhost.localdomain gunicorn[1420]: [2020-05-20 13:29:50 +0700] [1434] [INFO] Booting worker with pid: 1434
May 20 13:29:50 localhost.localdomain gunicorn[1420]: [2020-05-20 13:29:50 +0700] [1435] [INFO] Booting worker with pid: 1435
May 20 13:29:50 localhost.localdomain gunicorn[1420]: [2020-05-20 13:29:50 +0700] [1436] [INFO] Booting worker with pid: 1436

```

## ลอง reboot พบว่าเปิดเซิฟอัตโนมัติได้
```
reboot
```
- [x] request 
- [x] status backend `sudo systemctl status climatebackend`
- [x] ตรวจ firewall forward port `sudo firewall-cmd --list-all`
