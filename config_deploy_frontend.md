# install nginx

https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-7


```
sudo firewall-cmd --permanent --zone=public --add-service=http
sudo firewall-cmd --reload

sudo systemctl start nginx
sudo systemctl status nginx
sudo systemctl enable nginx 
```

config nginx

```
cd  /etc/nginx

sudo nano nginx.conf
```

แก้
```
    server {
        listen       <port> default_server;
        listen       [::]:<port> default_server;
        server_name  _;
        root         /usr/share/nginx/html;
```

nginx จะ permission denied กับ port 4200

```
semanage port -l | grep http_port_t

semanage port -a -t http_port_t  -p tcp 4200

หรือเป็น -m -t ... ถ้ามีพอร์ตแล้ว

```


## deploy angular

https://medium.com/@hmurari/serving-angular-app-from-behind-a-nginx-web-server-3579df0b04b1

```
sudo firewall-cmd --zone=public --add-port=4200/tcp --permanent
sudo firewall-cmd --reload
```

เข้า url server จะเจอหน้าเว็ป nginx